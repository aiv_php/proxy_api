<?php
/**
 * Класс для обработки запроса API от клиента, его перенаправление, получения ответа и записи результатов в файл журнала.
 *
 * При создании экземпляра класса автоматически выполняются все настройки для отправки запроса на API.
 * URL API и файл для записи журнала устанавливаются из файла конфигураций "config.php" в корневой папке проекта.
 * Параметры запроса, заголовки, URL API, файл для записи можно так-же настраивать индивидуально с помощью соответствующих методов класса.
 * Что нужно для отправки запроса и получения ответа:
 *
 * $api = new \Classes\Api(); Создаем экземпляр класса.
 * $api->goRequest(); Выполняем запрос.
 * echo $api->getResponse(); Выводим ответ.
 *
 * Created by PhpStorm.
 * User: CoDeR
 * Date: 22.08.2017
 * Time: 15:43
 */

namespace Classes;

class Api
{

    # URL API для перенаправления запроса
    private $url;

    # Метод запроса "POST" или "GET" (по умолчанию "POST")
    private $request_method = 'POST';

    # Переменная массив для получения данных запроса
    private $request = [];

    # Переменная массив для получения заголовков
    private $headers = [];

    # Переменная массив для получения данных ответа
    private $response = [];

    # Название файла для записи данных запроса и ответа
    private $file;

    /**
     * Конструктор.
     * Установка URL API с файла конфигураций;
     * Установка файла для записи журнала с файла конфигураций;
     * Автоопределение метода запроса $request_method;
     * Автоопределение параметров запроса $request;
     * Автоопределение параметров заголовка $headers.
     */
    public function __construct()
    {
        $config = require(__DIR__ . '/../config.php');

        if(!empty($config['url'])){
            $this->url = $config['url'];
        }

        if(!empty($config['file_name'])){
            $this->file = $config['file_name'];
        }

        $this->autoSetMethod();

        $request = $_REQUEST;
        if (count($request) > 0) {
            $this->setParameters($request);
        }

        $headers = getallheaders();
        if (count($headers) > 0) {
            $this->setHeaders($headers);
        }
    }

    /**
     * Автоопределение метода запроса
     */
    private function autoSetMethod()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $this->request_method = 'POST';
        } else {
            $this->request_method = 'GET';
        }
    }

    /**
     * Получение параметров запроса
     *
     * @param array $request Параметры запроса
     */
    public function setParameters($request = [])
    {
        $this->request = $request;
    }

    /**
     * Получение параметров заголовка
     *
     * @param array $headers Параметры заголовка
     */
    public function setHeaders($headers = [])
    {
        $this->headers = $headers;
    }

    /**
     * Установка URL сервера
     *
     * @param string $url URL адрес
     */
    public function setApiUrl($url = '')
    {
        if (!empty($url)) {
            $this->url = $url;
        }
    }

    /**
     * Установка имени файла для записи лога
     *
     * @param string $file файл для записи лога
     */
    public function setFileToSave($file = '')
    {
        if (!empty($file)) {
            $this->file = $file;
        }
    }

    /**
     * Вывод ответа c API сервера
     *
     * @param boolean $save запись запроса в лог true или false (по умолчанию true)
     *
     * @return array
     */
    public function getResponse($save = true)
    {
        if (!empty($save)) {
            $this->saveResponse();
        }

        return $this->response;
    }

    /**
     * Обработка и отправка запроса на API сервер
     */
    public function goRequest()
    {
        if ($this->request_method == 'POST') {
            $this->curlPost();
            return true;
        } elseif ($this->request_method == 'GET') {
            $this->curlGet();
            return true;
        } else {
            return false;
        }

    }

    /**
     * POST запросы по API
     */
    private function curlPost()
    {
        $url = $this->url;
        $defaults = array(
            CURLOPT_URL            => $url,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $this->request,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER     => $this->headers,
        );
        $curl = curl_init();
        curl_setopt_array($curl, $defaults);
        $out = curl_exec($curl);
        curl_close($curl);
        $this->response = $out;
    }

    /**
     * GET запросы по API
     */
    private function curlGet()
    {
        $url = $this->url;
        $strParams = '?';
        $parameters = is_array($this->request) && !empty($this->request)
            ? $this->request : [];
        foreach ($parameters as $key => $parameter) {
            $strParams = $strParams != '?' ? $strParams.'&' : $strParams;
            if ($parameter === '') {
                $strParams = $strParams.$key;
            } else {
                $strParams = $strParams.$key.'='.$parameter;
            }
        }
        $url = $url.$strParams;
        if ($curl = curl_init()) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
            $out = curl_exec($curl);
            $this->response = $out;
        }
    }

    /**
     * Запись в файл данных запроса и ответа
     */
    public function saveResponse()
    {
        $time = '[' . date('Y-m-d H:i:s') . ']';
        $from = $_SERVER['REMOTE_ADDR'] ? $_SERVER['REMOTE_ADDR'] : 'UNKNOWN IP';
        $method = $this->request_method . '_REQUEST';
        $request = json_encode($this->request);
        $request = $request ? $request : '(no set)';
        $response = $this->response;
        $response = $response ? $response : '(no result)';
        $string = $time . ' FROM ' . $from . ' BY ' . $method . ' REQUEST: ' . $request . ' RESPONSE: ' . $response;
        $file = fopen($this->file,"a+");
        fwrite($file, $string . "\r\n");
        fclose($file);
    }

    /**
     * Конвертирует асоциативный массив в строку
     *
     * @param array $array массив для обработки
     *
     * @return string
     */
    public static function arrayToString($array = [])
    {
        if (count($array) > 0) {
            $map = array_map(
                function ($k, $v) {
                    if ($v === ''){
                        return "$k";
                    }else{
                        return "$k = $v";
                    }
                },
                array_keys($array),
                $array
            );

            return implode(', ', $map);
        }
        return '(no set)';
    }
}