<?php

/**
 * Created by PhpStorm.
 * User: CoDeR
 * Date: 21.08.2017
 * Time: 16:07
 */

header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');

require_once ('Classes/Api.php');

$api = new \Classes\Api();

if($api->goRequest()){
    echo $api->getResponse();
}

